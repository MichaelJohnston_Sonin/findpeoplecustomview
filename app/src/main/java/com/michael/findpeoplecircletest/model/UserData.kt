package com.michael.findpeoplecircletest.model

class UserData(newUserName: String, newDistance: Int, newImage: Int){
    var userName = newUserName
    var distance = newDistance
    var image = newImage
}