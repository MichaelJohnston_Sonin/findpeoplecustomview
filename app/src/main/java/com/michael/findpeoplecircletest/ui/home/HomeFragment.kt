package com.michael.findpeoplecircletest.ui.home

import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.View.generateViewId
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintSet
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.michael.findpeoplecircletest.R
import com.michael.findpeoplecircletest.customview.FindPeopleView
import com.michael.findpeoplecircletest.customview.ProfileView
import com.michael.findpeoplecircletest.databinding.FragmentHomeBinding
import com.michael.findpeoplecircletest.model.UserData
import kotlinx.android.synthetic.main.profile_view.view.*
import java.security.SecureRandom
import kotlin.random.Random

class HomeFragment : Fragment() {

//    private val nearbyUsers = listOf(
//        UserData("John", 0, R.drawable.profile1),
//        UserData("Rosie", 0, R.drawable.profile2),
////        UserData("Megan", 0, R.drawable.profile3),
//        UserData("Jessica", 0, R.drawable.profile4),
//        UserData("Carl", 1, R.drawable.profile5),
//        UserData("Jade", 1, R.drawable.profile6),
//        UserData("Steve", 1, R.drawable.profile7),
//        UserData("Becca", 1, R.drawable.profile8),
//        UserData("Tom", 2, R.drawable.profile9),
//        UserData("Ted", 2, R.drawable.profile10),
////        UserData("Grace", 2, R.drawable.profile11),
//        UserData("Boris", 2, R.drawable.profile12)
//    )



    private val displayMetrics = DisplayMetrics()

    private val viewModel by viewModels<HomeViewModel> {
        // Get the width of the current device and give to viewModel
        activity?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)

        HomeViewModelFactory(requireContext().resources.displayMetrics.density, displayMetrics.widthPixels )
    }
    private lateinit var binding: FragmentHomeBinding

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_home,
            container,
            false
        )
        binding.viewModel = viewModel
        binding.lifecycleOwner = this.viewLifecycleOwner


        val secureRandom = SecureRandom()

        val nearbyUsers = listOf(
            UserData("John", secureRandom.nextInt(100), R.drawable.profile1),
            UserData("Rosie", secureRandom.nextInt(100), R.drawable.profile2),
            UserData("Megan", secureRandom.nextInt(100), R.drawable.profile3),
            UserData("Jessica", secureRandom.nextInt(100), R.drawable.profile4),
            UserData("Carl", secureRandom.nextInt(100), R.drawable.profile5),
            UserData("Jade", secureRandom.nextInt(100), R.drawable.profile6),
            UserData("Steve", secureRandom.nextInt(100), R.drawable.profile7),
            UserData("Becca", secureRandom.nextInt(100), R.drawable.profile8),
            UserData("Tom", secureRandom.nextInt(100), R.drawable.profile9),
            UserData("Ted", secureRandom.nextInt(100), R.drawable.profile10),
            UserData("Grace", secureRandom.nextInt(100), R.drawable.profile11),
            UserData("Boris", secureRandom.nextInt(100), R.drawable.profile12)
        )

        val currentUser = nearbyUsers[secureRandom.nextInt(nearbyUsers.size-1)]

        /*
        * Can insert how ever many nearby users into FindPeopleView() as you like, but will only ever display maximum 12 people.
        * 4 profile images per circle radius.
        * Distance currently ranges between 1..100
        * */
        binding.llMain.addView(FindPeopleView(requireContext(), currentUser, nearbyUsers, displayMetrics))

        return binding.root
    }

}
