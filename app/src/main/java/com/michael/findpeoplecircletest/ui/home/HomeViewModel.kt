package com.michael.findpeoplecircletest.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.michael.findpeoplecircletest.R
import com.michael.findpeoplecircletest.model.Coors
import com.michael.findpeoplecircletest.model.ProfilePosition
import com.michael.findpeoplecircletest.model.UserData
import kotlin.math.cos
import kotlin.math.roundToInt
import kotlin.math.sin

class HomeViewModelFactory(
    private val density: Float,
    private val width: Int
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(
            Float::class.java,
            Int::class.java
        ).newInstance(density, width)
    }
}

class HomeViewModel(private val screenDensity: Float, private val screenWidth: Int) : ViewModel() {

    var profilePositions = MutableLiveData<List<ProfilePosition>>()

    private var firstCircleDiameter = (screenWidth*0.9).toInt()
    private var secondCircleDiameter = (screenWidth*1.4).toInt()
    private var thirdCircleDiameter = (screenWidth*1.8).toInt()

    private val nearbyUsers = listOf(
        UserData("John", 0, R.drawable.profile1),
        UserData("Rosie", 0, R.drawable.profile2),
//        UserData("Megan", 0, R.drawable.profile3),
        UserData("Jessica", 0, R.drawable.profile4),
        UserData("Carl", 1, R.drawable.profile5),
        UserData("Jade", 1, R.drawable.profile6),
        UserData("Steve", 1, R.drawable.profile7),
        UserData("Becca", 1, R.drawable.profile8),
        UserData("Tom", 2, R.drawable.profile9),
        UserData("Ted", 2, R.drawable.profile10),
//        UserData("Grace", 2, R.drawable.profile11),
        UserData("Boris", 2, R.drawable.profile12))

    init {
        profilePositions.value = getProfilePositions(nearbyUsers)
    }

    private fun getProfilePositions(users: List<UserData>): List<ProfilePosition>{
        val tempProfilePositions = arrayListOf<ProfilePosition>()

        // Separate users into relevant circles
        val circleUserSeparation = arrayListOf<ArrayList<UserData>>(arrayListOf(), arrayListOf(), arrayListOf())
        users.map { circleUserSeparation[it.distance].add(it) }

        circleUserSeparation.forEachIndexed{circleIndex, circleUsers ->
            if(circleUsers.size > 0){
                val separationAngles = getDegreesPosition(circleUsers.size, circleIndex)

                val circleRadius = getCircleRadius(circleIndex)


                circleUsers.forEachIndexed { index, userData ->
                    val angleMax: Int
                    val angleMin: Int
                    val curAngle: Double

                    when(circleUsers.size){
                        1 -> {
                            angleMax = (separationAngles[index] + 15).toInt()
                            angleMin = (separationAngles[index] - 15).toInt()
                            curAngle =  (angleMin..angleMax).random() * Math.PI/180
                        }
                        2 -> {
                            angleMax = (separationAngles[index] + 10).toInt()
                            angleMin = (separationAngles[index] - 10).toInt()
                            curAngle =  (angleMin..angleMax).random() * Math.PI/180
                        }
                        3 -> {
                            angleMax = (separationAngles[index] + 5).toInt()
                            angleMin = (separationAngles[index] - 5).toInt()
                            curAngle =  (angleMin..angleMax).random() * Math.PI/180
                        }
                        else->{
                            curAngle =  separationAngles[index] * Math.PI/180
                        }
                    }

                    val translateByX = calculateTranslationX(curAngle, circleRadius)
                    var translateByY = calculateTranslationY(curAngle, circleRadius)

                    translateByY = if(translateByY < 0){
                        translateByY
                    }else{
                        -translateByY
                    }

                    tempProfilePositions.add(ProfilePosition(Coors(translateByX, translateByY), userData))
                }
            }
        }
        return tempProfilePositions
    }

    private fun getCircleRadius(index: Int): Int{
        return when(index){
            0 -> firstCircleDiameter/2
            1 -> secondCircleDiameter/2
            2 -> thirdCircleDiameter/2
            else -> thirdCircleDiameter/2
        }
    }

    private fun getDegreesPosition(amountOfProfiles: Int, circleIndex: Int): List<Double>{
        return when(circleIndex) {
            0 ->  when(amountOfProfiles){
                1 -> listOf(90.0)
                2 -> listOf(60.0, 120.0)
                3 -> listOf(40.0, 90.0, 140.0)
                4 -> listOf(30.0, 70.0, 110.0, 150.0)
                else -> listOf()
            }
            1 -> when(amountOfProfiles){
                1 -> listOf(90.0)
                2 -> listOf(70.0, 110.0)
                3 -> listOf(60.0, 90.0, 120.0)
                4 -> listOf(60.0, 80.0, 100.0, 120.0)
                else -> listOf()
            }
            2-> when(amountOfProfiles){
                1 -> listOf(90.0)
                2 -> listOf(75.0, 105.0)
                3 -> listOf(65.0, 90.0, 115.0)
                4 -> listOf(65.0, 80.0, 100.0, 115.0)
                else -> listOf()
            }
            else ->
                when(amountOfProfiles){
                    1 -> listOf(90.0)
                    2 -> listOf(70.0, 110.0)
                    3 -> listOf(50.0, 90.0, 130.0)
                    4 -> listOf(40.0, 70.0, 110.0, 150.0)
                    else -> listOf()
                }
        }
    }

    private fun calculateTranslationX(angle: Double, radius: Int): Float {
        return (radius * cos(angle)).toFloat()
    }

    private fun calculateTranslationY(angle: Double, radius: Int): Float {
        return (radius * sin(angle)).toFloat()
    }

    fun dpToPx(dp: Float): Float {
        return (dp * screenDensity).roundToInt().toFloat()
    }
}