package com.michael.findpeoplecircletest.customview

import android.content.Context
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.makeramen.roundedimageview.RoundedImageView
import com.michael.findpeoplecircletest.R
import com.michael.findpeoplecircletest.model.UserData

class ProfileView(context: Context, userData: UserData): LinearLayout(context){
    init {
        View.inflate(context, R.layout.profile_view, this)

        val userName: TextView = findViewById(R.id.user_name)
        val profileImage: RoundedImageView = findViewById(R.id.user_image)

        profileImage.setImageResource(userData.image)
        userName.text = userData.userName
    }
}