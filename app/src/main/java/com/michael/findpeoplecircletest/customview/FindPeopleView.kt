package com.michael.findpeoplecircletest.customview

import android.content.Context
import android.os.Build
import android.util.DisplayMetrics
import android.view.View
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import com.michael.findpeoplecircletest.R
import com.michael.findpeoplecircletest.model.Coors
import com.michael.findpeoplecircletest.model.ProfilePosition
import com.michael.findpeoplecircletest.model.UserData
import kotlinx.android.synthetic.main.find_people_view.view.*
import kotlinx.android.synthetic.main.profile_view.view.*
import kotlin.math.cos
import kotlin.math.sin

@RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
class FindPeopleView(
    context: Context,
    private val currentUserData: UserData,
    private val nearByUsers: List<UserData>,
    private val displayMetrics: DisplayMetrics
) : LinearLayout(context) {
    private val constraintLayout: ConstraintLayout

    private val screenWidth = displayMetrics.widthPixels
    private val screenHeight = displayMetrics.heightPixels

    // Set circle diameters
    private val firstCircleDiameter = (screenWidth * 0.9).toInt()
    private val secondCircleDiameter = (screenWidth * 1.4).toInt()
    private val thirdCircleDiameter = (screenWidth * 1.8).toInt()

    // Move circles and profiles down
    private val shiftYBy = (screenWidth * 0.1).toFloat()
    private val nearByProfileImageSize = (screenWidth * 0.11).toInt()
    private val shiftProfilesBy = (nearByProfileImageSize * 0.75).toFloat()

    init {
        View.inflate(context, R.layout.find_people_view, this)

        constraintLayout = findViewById(R.id.cl_find_people)
        constraintLayout.layoutParams.height = thirdCircleDiameter / 2

        setUpCircleSizesAndMainProfileImage()
        placeNearByUsersProfiles(processNearByUsers())
    }

    /* Sets the size of each circle according to the width of the current devices screen, also sets
    *  users profile image resource and size. After this shifts circles down a bit to offset the center
    *  from the users profile image.  */
    private fun setUpCircleSizesAndMainProfileImage() {
        // Set sizes of circles depending on screen size
        constraintLayout.circle1.layoutParams.height = (screenWidth * 0.9).toInt()
        constraintLayout.circle1.layoutParams.width = (screenWidth * 0.9).toInt()

        constraintLayout.circle2.layoutParams.height = (screenWidth * 1.4).toInt()
        constraintLayout.circle2.layoutParams.width = (screenWidth * 1.4).toInt()

        constraintLayout.circle3.layoutParams.height = (screenWidth * 1.8).toInt()
        constraintLayout.circle3.layoutParams.width = (screenWidth * 1.8).toInt()

        // Set size of central main profile image
        constraintLayout.center_profile_image.layoutParams.height = (screenWidth * 0.18).toInt()
        constraintLayout.center_profile_image.layoutParams.width = (screenWidth * 0.18).toInt()

        // Place users own profile image in center profile
        constraintLayout.center_profile_image.setImageResource(currentUserData.image)

        // Clone constraint layout to move circles down
        val editCircleSet = ConstraintSet()
        var vl = findViewById<ConstraintLayout>(R.id.cl_find_people)
        editCircleSet.clone(vl)

        // Translate position of circles
        editCircleSet.setTranslationY(R.id.circle1, shiftYBy)
        editCircleSet.setTranslationY(R.id.circle2, shiftYBy)
        editCircleSet.setTranslationY(R.id.circle3, shiftYBy)

        editCircleSet.applyTo(vl)
    }

    // Currently distance range is limited between 1..100
    private fun checkDistanceGroup(distance: Int): Int {
        return when {
            (distance in 1..33) -> 0
            (distance in 34..66) -> 1
            (distance in 67..100) -> 2
            else -> 0
        }
    }

    private fun placeNearByUsersProfiles(nearByProfilePositions: List<ProfilePosition>) {
        nearByProfilePositions.forEach { profilePosition ->
            val profileView = ProfileView(context, profilePosition.userData)
            profileView.id = generateViewId()

            profileView.user_image.layoutParams.height = nearByProfileImageSize
            profileView.user_image.layoutParams.width = nearByProfileImageSize

            profileView.user_image.setImageResource(profilePosition.userData.image)

            constraintLayout.addView(profileView)

            val set = ConstraintSet()
            set.clone(constraintLayout)

            // Connect the profile view inside the constraint view to its parent layout
            set.connect(
                profileView.id,
                ConstraintSet.BOTTOM,
                constraintLayout.id,
                ConstraintSet.BOTTOM,
                0
            )
            set.connect(
                profileView.id,
                ConstraintSet.END,
                constraintLayout.id,
                ConstraintSet.END,
                0
            )
            set.connect(
                profileView.id,
                ConstraintSet.START,
                constraintLayout.id,
                ConstraintSet.START,
                0
            )
            set.connect(
                profileView.id,
                ConstraintSet.TOP,
                constraintLayout.id,
                ConstraintSet.TOP,
                0
            )
            set.setVerticalBias(profileView.id, 1F)

            // Move profile view onto designated circle, with pre-calculated x and y translation values
            set.setTranslationX(profileView.id, profilePosition.coors.x)
            set.setTranslationY(
                profileView.id,
                profilePosition.coors.y + shiftProfilesBy + shiftYBy
            )

            // Apply all this to layout and move onto next profile view
            set.applyTo(constraintLayout)
        }
    }

    private fun processNearByUsers(): List<ProfilePosition> {
        val tempProfilePositions = arrayListOf<ProfilePosition>()

        // Separate users into relevant circles
        val circleUserSeparation =
            arrayListOf<ArrayList<UserData>>(arrayListOf(), arrayListOf(), arrayListOf())

        // TODO: Shuffled for variety feature can be removed, just for demo purposes
        // Max group number is 4 per circle
        nearByUsers.shuffled().map {
            val distanceGroup = checkDistanceGroup(it.distance)
            if(circleUserSeparation[distanceGroup].size < 4){
                circleUserSeparation[distanceGroup].add(it)
            }
        }

        circleUserSeparation.forEachIndexed { circleIndex, circleUsers ->
            if (circleUsers.size > 0) {
                val separationAngles = getDegreesPosition(circleUsers.size, circleIndex)

                val circleRadius = getCircleRadius(circleIndex)


                circleUsers.forEachIndexed { index, userData ->
                    val angleMax: Int
                    val angleMin: Int
                    val curAngle: Double

                    when (circleUsers.size) {
                        1 -> {
                            angleMax = (separationAngles[index] + 15).toInt()
                            angleMin = (separationAngles[index] - 15).toInt()
                            curAngle = (angleMin..angleMax).random() * Math.PI / 180
                        }
                        2 -> {
                            angleMax = (separationAngles[index] + 10).toInt()
                            angleMin = (separationAngles[index] - 10).toInt()
                            curAngle = (angleMin..angleMax).random() * Math.PI / 180
                        }
                        3 -> {
                            angleMax = (separationAngles[index] + 3).toInt()
                            angleMin = (separationAngles[index] - 3).toInt()
                            curAngle = (angleMin..angleMax).random() * Math.PI / 180
                        }
                        else -> {
                            curAngle = separationAngles[index] * Math.PI / 180
                        }
                    }

                    val translateByX = calculateTranslationX(curAngle, circleRadius)
                    var translateByY = calculateTranslationY(curAngle, circleRadius)

                    translateByY = if (translateByY < 0) {
                        translateByY
                    } else {
                        -translateByY
                    }

                    tempProfilePositions.add(
                        ProfilePosition(
                            Coors(translateByX, translateByY),
                            userData
                        )
                    )
                }
            }
        }
        return tempProfilePositions
    }

    private fun getCircleRadius(index: Int): Int {
        return when (index) {
            0 -> firstCircleDiameter / 2
            1 -> secondCircleDiameter / 2
            2 -> thirdCircleDiameter / 2
            else -> thirdCircleDiameter / 2
        }
    }

    private fun getDegreesPosition(amountOfProfiles: Int, circleIndex: Int): List<Double> {
        return when (circleIndex) {
            0 -> when (amountOfProfiles) {
                1 -> listOf(90.0)
                2 -> listOf(60.0, 120.0)
                3 -> listOf(40.0, 90.0, 140.0)
                4 -> listOf(30.0, 70.0, 110.0, 150.0)
                else -> listOf()
            }
            1 -> when (amountOfProfiles) {
                1 -> listOf(90.0)
                2 -> listOf(70.0, 110.0)
                3 -> listOf(60.0, 90.0, 120.0)
                4 -> listOf(60.0, 80.0, 100.0, 120.0)
                else -> listOf()
            }
            2 -> when (amountOfProfiles) {
                1 -> listOf(90.0)
                2 -> listOf(75.0, 105.0)
                3 -> listOf(65.0, 90.0, 115.0)
                4 -> listOf(65.0, 80.0, 100.0, 115.0)
                else -> listOf()
            }
            else ->
                when (amountOfProfiles) {
                    1 -> listOf(90.0)
                    2 -> listOf(70.0, 110.0)
                    3 -> listOf(50.0, 90.0, 130.0)
                    4 -> listOf(40.0, 70.0, 110.0, 150.0)
                    else -> listOf()
                }
        }
    }

    private fun calculateTranslationX(angle: Double, radius: Int): Float {
        return (radius * cos(angle)).toFloat()
    }

    private fun calculateTranslationY(angle: Double, radius: Int): Float {
        return (radius * sin(angle)).toFloat()
    }
}